# Sum of Powers #

Consider a natural number n > 1 and a natural exponent q > 1 not greater than 10000. Given q find a minimal n such that sum over k of n_k^q mod 2^64 = n where n_k is a kth digit of n and ^ is the power operator.

More details can be found in the "sum_of_powers.pdf" document.
