# 20160219 felix_kott
#
# Consider a natural number n > 0 and a natural exponent q > 0 not greater
# than 10000. Given q find a minimal n such that
# sum over k of n_k^q mod 2^64 = n where
# n_k is the kth digit of n and ^ is a power operator.
#
# More details in the "sum_of_powers.pdf".

import fileinput

q_list = []

for line in fileinput.input():  # sum_of_powers.input_data : integers per line
    q_list.append(int(line.strip()))

q_precalculated = {
    3: 153,
    4: 8208,
    5: 4150,
    6: 548834,
    7: 9800817,
    8: 24678050,
    9: 146511208,
    10: 4679307774,
    11: 32164049650,
    13: 564240140138,
    14: 28116440335967,
    16: 4338281769391370,
    17: 233411150132317,
    19: 1517841543307505039,
    21: 18202327678379096573,
    23: 1603443512238388563,
    25: 13498352190417151359,
    26: 6475898051276910200,
    27: 1333582124929118770,
    28: 16932243450297313082,
    30: 4780310504938639624,
    31: 9688633288883680759,
    32: 2043377144301396747,
    34: 3666547427833842577,
    36: 3034828013568944487,
    39: 10125023131941604162,
    40: 4522690561679346345,
    41: 12709081227525085805,
    43: 18129480408439192765,
    44: 13850147106324834939,
    45: 13033954293523995989,
    46: 17023385356256359180,
    48: 13780204543050143625,
    49: 3672209887935102392,
    53: 10425708809908288278,
    54: 17686242077775456488,
    56: 11689044021234624870,
    58: 17253514239882627347,
    59: 4790856675533572808,
    60: 3055613188127075083,
    61: 7934058467924595525
    }

m = 0xffffffffffffffff  # 64-bit mask

for q in q_list:
    if q in q_precalculated:
        print(q_precalculated[q])
    elif q < 65:
        print("")
    else:
        r = 99999999999999999999  # old trick with maximum value...
        f = False
        q_power = [(d**q & m) for d in range(10)]
        # q_power = array of precalculated q-th exponents
        core = {0: 0, 1: 1}  # sequence_core...
        sequence = [  # = n; start with 3 to skip trivial cases
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, 3]
        while sequence[0] != 1 or sequence[1] != 7 or sequence[2] != 9:
            while sequence[19] < 10:
                # i = sequence.index(filter(lambda x: x != -1, sequence)[0])
                # get first index of non-zero element
                i = next((j for j, v in enumerate(sequence) if v != -1), None)
                key = "0" + "".join([str(w) for w in sequence[i: 19]])
                # add "0" to serve warming up the sequence
                # or equivalently : ... h = h if h else [0]
                next_key = int(key + str(sequence[19]))
                s = core[next_key] = core[int(key)] + q_power[sequence[19]] & m
                if s > 1 and s == sum([q_power[int(c)] for c in str(s)]) & m:
                    if s <= r:  # get minimal value
                        r = s
                        f = True
                sequence[19] += 2  # skip all even numbers!
            i = sequence.index(11) - 1
            sequence[i:] = [sequence[i] + 2] * (20 - i)
#           get next sequence value skipping all decreasing ones
#           and excluding even digits!
        if not f:
            print("")
        else:
            print(r)

"""
# former version - used for q less than 65
# ========================================

import sys
from decimal import *
from collections import Counter
context = Context(prec = 20) # decimal precision = 20
m = 18446744073709551616 # 2**64
q = int(sys.argv[1])
m = 0xffffffffffffffff  # 64-bit mask
q_power = [(d**q & m) for d in xrange(10)]
sequence_core = { 0: 0, 1: 1 }
sequence = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2]
unit_index = 19
v = -1
while v < m: # lack of do-while loop results in a few extra runs...
    while sequence[unit_index] < 10:
        t = unit_index
        while v != 0 and t:
            v = sequence[t]
            t -= 1
        h = sequence[t + 2: 19]
        key = "0" + "".join([str(w) for w in h])
        next_key = int(key + str(sequence[unit_index]))
        s = (sequence_core[int(key)] + q_power[sequence[unit_index]]) & m
        sequence_core[next_key] = int(s)
        z = Counter(str(s))["0"]
        b = sequence[t + 2 - z: 20]
        v = int("".join([str(w) for w in b]))
        if s > 1 and int(s) == sum([q_power[int(c)] for c in str(s)]) & m:
            print(sequence)
            print({q: s})
            #print("\033[92m" + "found: %s") % ({q: int(s)})
            #print(sequence)
            sys.exit(0)
        sequence[unit_index] += 1 # increase unit digits
    i = sequence.index(10) - 1
    next_value = sequence[i] + 1
    for k in range(i, 20):
        sequence[k] = next_value
"""
